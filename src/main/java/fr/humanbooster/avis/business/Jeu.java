package fr.humanbooster.avis.business;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Jeu {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	@NotBlank(message="Le nom ne peut pas être vide.")
	private String nom;
	
	@Column(nullable = false)
	@NotBlank(message="La description ne peut pas être vide.")
	private String description;
	
	@Past(message="La date doit être dans le passé.")
	private Date dateSortie;
	
	private float noteMoyenne;
	
	@NotNull(message="Une option doit être sélectionnée.")
	@ManyToOne
	private Editeur editeur;
	
	@NotNull(message="Une option doit être sélectionnée.")
	@ManyToOne
	private ModeleEconomique modeleEconomique;

	@NotNull(message="Une option doit être sélectionnée.")
	@ManyToOne
	private Classification classification;
	
	@NotNull(message="Une option doit être sélectionnée.")
	@ManyToOne
	private Genre genre;
	
	@NotNull
	@NotEmpty(message="Une option doit être sélectionnée.")
	@ManyToMany
	private List<Plateforme> plateformes;
	
	@JsonIgnore
	@OneToMany(mappedBy="jeu")
	private List<Avis> avis;
	
	public Jeu() {
	}

	public Jeu(@NotBlank(message = "Le nom ne peut pas être vide.") String nom,
			@NotBlank(message = "La description ne peut pas être vide.") String description,
			@Past(message = "La date doit être dans le passé.") Date dateSortie, @NotNull Editeur editeur,
			@NotNull ModeleEconomique modeleEconomique, @NotNull Classification classification, @NotNull Genre genre,
			@NotNull List<Plateforme> plateformes) {
		super();
		this.nom = nom;
		this.description = description;
		this.dateSortie = dateSortie;
		this.editeur = editeur;
		this.modeleEconomique = modeleEconomique;
		this.classification = classification;
		this.genre = genre;
		this.plateformes = plateformes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateSortie() {
		return dateSortie;
	}

	public void setDateSortie(Date dateSortie) {
		this.dateSortie = dateSortie;
	}

	public Editeur getEditeur() {
		return editeur;
	}

	public void setEditeur(Editeur editeur) {
		this.editeur = editeur;
	}

	public ModeleEconomique getModeleEconomique() {
		return modeleEconomique;
	}

	public void setModeleEconomique(ModeleEconomique modeleEconomique) {
		this.modeleEconomique = modeleEconomique;
	}

	public Classification getClassification() {
		return classification;
	}

	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public List<Plateforme> getPlateformes() {
		return plateformes;
	}

	public void setPlateformes(List<Plateforme> plateformes) {
		this.plateformes = plateformes;
	}

	public List<Avis> getAvis() {
		return avis;
	}

	public void setAvis(List<Avis> avis) {
		this.avis = avis;
	}

	public float getNoteMoyenne() {
		return noteMoyenne;
	}

	public void setNoteMoyenne(float noteMoyenne) {
		this.noteMoyenne = noteMoyenne;
	}

	@Override
	public String toString() {
		return "Jeu [id=" + id + ", nom=" + nom + ", description=" + description + ", dateSortie=" + dateSortie
				+ ", editeur=" + editeur + ", modeleEconomique=" + modeleEconomique + ", classification="
				+ classification + ", genre=" + genre + "]";
	}

	
}
