package fr.humanbooster.avis.business;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class Avis {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Date dateEnvoi;
	
	@Min(value=0, message="La note ne peut pas être inférieur à 0.")
	@Max(value=20, message="La note ne peut pas être supérieur à 20.")
	private Float note;
	
	@Column(nullable = false)
	@NotBlank(message="La description ne peut pas être vide.")
	@Size(min=3, message="La description doit faire plus de 3 caractères")
	private String description;
	
	@ManyToOne
	private Jeu jeu;
	
	@ManyToOne
	private Joueur joueur;
	
	public Avis() {
	}

	public Avis(Date dateEnvoi,
			@Min(value = 0, message = "La note ne peut pas être inférieur à 0.") @Max(value = 20, message = "La note ne peut pas être supérieur à 20.") Float note,
			@NotBlank(message = "La description ne peut pas être vide.") @Size(min = 3, message = "La description doit faire plus de 3 caractères") String description,
			Jeu jeu, Joueur joueur) {
		super();
		this.dateEnvoi = dateEnvoi;
		this.note = note;
		this.description = description;
		this.jeu = jeu;
		this.joueur = joueur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public Float getNote() {
		return note;
	}

	public void setNote(Float note) {
		this.note = note;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Jeu getJeu() {
		return jeu;
	}

	public void setJeu(Jeu jeu) {
		this.jeu = jeu;
	}

	public Joueur getJoueur() {
		return joueur;
	}

	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}

	@Override
	public String toString() {
		return "Avis [id=" + id + ", dateEnvoi=" + dateEnvoi + ", note=" + note + ", description=" + description
				+ ", jeu=" + jeu + ", joueur=" + joueur + "]";
	}
	
}
