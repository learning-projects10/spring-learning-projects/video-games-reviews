package fr.humanbooster.avis.controller;

import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import fr.humanbooster.avis.business.Avis;
import fr.humanbooster.avis.business.Classification;
import fr.humanbooster.avis.business.Editeur;
import fr.humanbooster.avis.business.Genre;
import fr.humanbooster.avis.business.Jeu;
import fr.humanbooster.avis.business.Joueur;
import fr.humanbooster.avis.business.ModeleEconomique;
import fr.humanbooster.avis.business.Plateforme;
import fr.humanbooster.avis.service.AvisService;
import fr.humanbooster.avis.service.ClassificationService;
import fr.humanbooster.avis.service.EditeurService;
import fr.humanbooster.avis.service.GenreService;
import fr.humanbooster.avis.service.JeuService;
import fr.humanbooster.avis.service.JoueurService;
import fr.humanbooster.avis.service.ModeleEconomiqueService;
import fr.humanbooster.avis.service.PlateformeService;

@Controller
@RequestMapping("/")
public class mainController {

	private HttpSession httpSession;
	private JoueurService joueurService;
	private EditeurService editeurService;
	private ClassificationService classificationService;
	private GenreService genreService;
	private PlateformeService plateformeService;
	private ModeleEconomiqueService modeleEconomiqueService;
	private JeuService jeuService;
	private AvisService avisService;
	
	private static final int NB_PAR_PAGE = 5;

	public mainController(HttpSession httpSession, JoueurService joueurService, EditeurService editeurService,
			ClassificationService classificationService, GenreService genreService, PlateformeService plateformeService,
			ModeleEconomiqueService modeleEconomiqueService, JeuService jeuService, AvisService avisService) {
		super();
		this.httpSession = httpSession;
		this.joueurService = joueurService;
		this.editeurService = editeurService;
		this.classificationService = classificationService;
		this.genreService = genreService;
		this.plateformeService = plateformeService;
		this.modeleEconomiqueService = modeleEconomiqueService;
		this.jeuService = jeuService;
		this.avisService = avisService;
	}

	//===============================================================================
	//=================================== Index =====================================
	@RequestMapping(value = {"/", "/index"})
	public ModelAndView index() {
		Joueur joueur = (Joueur) httpSession.getAttribute("joueur");
		if (joueur != null) {
			ModelAndView mav = new ModelAndView("redirect:accueil");
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("index");
			return mav;
		}
	}
	
	//===============================================================================
	//================================== Connexion ==================================
	@PostMapping("/connexion")
	public ModelAndView connexionPost(@RequestParam("PSEUDO") String pseudo, @RequestParam("MOT_DE_PASSE") String motDePasse) {
		Joueur joueur = joueurService.recupererJoueurByPseudoAndMotDePasse(pseudo.toLowerCase(), motDePasse);
		if (joueur != null) {
			ModelAndView mav = new ModelAndView("redirect:accueil");
			httpSession.setAttribute("joueur", joueur);
			return mav;
		} else {
			ModelAndView mav = index();
			mav.addObject("notification", "Le mot de passe et/ou l'identifiant est incorrect.");
			return mav;
		}
	}
	
	//===============================================================================
	//=================================== Accueil ===================================
	@GetMapping("/accueil")
	public ModelAndView accueilGet(@PageableDefault(size = NB_PAR_PAGE) Pageable pageable) {
		Joueur joueur = (Joueur) httpSession.getAttribute("joueur");
		if (joueur != null) {
			ModelAndView mav = new ModelAndView("lesAvis");
			mav.addObject("avisParPages", avisService.recupererAviss(pageable));
			
	        Sort.Order order = null;
	        Sort sort = pageable.getSort();
	        if (sort != null) {
	             if (sort.iterator().hasNext()) {
	                 order = sort.iterator().next();
	             }
	        }
	        
	        if (order != null) {
	             mav.addObject("sort", order.getProperty() + "," + order.getDirection().toString());
	        }
	        
	        return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:/");
			return mav;
		}
	}
	
	//===============================================================================
	//==================================== Avis =====================================
	@GetMapping("/ajouter-avis")
	public ModelAndView ajouterAvisGet() {
		Joueur joueur = (Joueur) httpSession.getAttribute("joueur");
		if (joueur != null) {
			ModelAndView mav = new ModelAndView("avis");
			mav.addObject("jeux", jeuService.recupererJeux());
			mav.addObject("avis", new Avis());
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:/");
			return mav;
		}	
	}
	
	@PostMapping("/ajouter-avis")
	public ModelAndView ajouterAvisPost(@Valid @ModelAttribute Avis avis, BindingResult result) {
		Joueur joueur = (Joueur) httpSession.getAttribute("joueur");
		if (joueur != null) {
			if (result.hasErrors()) {
				ModelAndView mav = ajouterAvisGet();
				mav.addObject("avis", avis);
				return mav;
			}
			else  {
				avis.setJoueur(joueur);
				avis.setDateEnvoi(new Date());
				avisService.creerAvis(avis);
				ModelAndView mav = new ModelAndView("redirect:accueil");
				return mav;
			}
		} else {
			ModelAndView mav = new ModelAndView("redirect:/");
			return mav;
		}		
	}
	
	//===============================================================================
	//=============================== liste des jeux ================================
	@GetMapping("/liste-jeux")
	public ModelAndView listeJeuGet(@PageableDefault(size = NB_PAR_PAGE) Pageable pageable) {
		Joueur joueur = (Joueur) httpSession.getAttribute("joueur");
		if (joueur != null) {
			ModelAndView mav = new ModelAndView("lesJeux");
			mav.addObject("jeuxParPages", jeuService.recupererJeux(pageable));
			
	        Sort.Order order = null;
	        Sort sort = pageable.getSort();
	        if (sort != null) {
	             if (sort.iterator().hasNext()) {
	                 order = sort.iterator().next();
	             }
	        }
	        
	        if (order != null) {
	             mav.addObject("sort", order.getProperty() + "," + order.getDirection().toString());
	        }
	        
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:/");
			return mav;
		}	
	}
	
	//===============================================================================
	//================================ Ajouter jeu ==================================
	@GetMapping("/ajouter-jeu")
	public ModelAndView ajouterJeuGet() {
		Joueur joueur = (Joueur) httpSession.getAttribute("joueur");
		if (joueur != null) {
			ModelAndView mav = new ModelAndView("jeu");
			Jeu jeu = new Jeu();
			jeu.setDateSortie(new Date());
			mav.addObject("jeu", jeu);
			mav.addObject("editeurs", editeurService.recupererEditeurs());
			mav.addObject("genres", genreService.recupererGenres());
			mav.addObject("classifications", classificationService.recupererClassifications());
			mav.addObject("plateformes", plateformeService.recupererPlateformes());
			mav.addObject("modelesEconomique", modeleEconomiqueService.recupererModelesEconomique());
			return mav;
		} else {
			ModelAndView mav = new ModelAndView("redirect:/");
			return mav;
		}	
	}
	
	@PostMapping("/ajouter-jeu")
	public ModelAndView ajouterJeuPost(@Valid @ModelAttribute Jeu jeu, BindingResult result) {
		Joueur joueur = (Joueur) httpSession.getAttribute("joueur");
		if (joueur != null) {
			if (result.hasErrors()) {
				ModelAndView mav = ajouterJeuGet();
				mav.addObject("jeu", jeu);
				return mav;
			}
			else  {
				jeuService.creerJeu(jeu);
				ModelAndView mav = new ModelAndView("redirect:/liste-jeux");
				return mav;
			}
		} else {
			ModelAndView mav = new ModelAndView("redirect:/");
			return mav;
		}		
	}
	
	//===============================================================================
	//================================= deconnexion =================================
	@GetMapping("/deconnexion")
	public ModelAndView deconnexion() {
		httpSession.invalidate();
		ModelAndView mav = new ModelAndView("redirect:/");
		return mav;
	}
	
	//===============================================================================
	//============================== init / initBinder ==============================
	@PostConstruct
	private void init() {
		if (editeurService.recupererEditeurs().isEmpty()) {
			editeurService.creerEditeur(new Editeur("Epic Games"));
			editeurService.creerEditeur(new Editeur("Activision"));
		}
		
		if (genreService.recupererGenres().isEmpty()) {
			genreService.creerGenre(new Genre("FPS"));
			genreService.creerGenre(new Genre("Battle Royal"));
		}
		
		if (classificationService.recupererClassifications().isEmpty()) {
			classificationService.creerClassification(new Classification("PEGI 12"));
			classificationService.creerClassification(new Classification("PEGI 16"));
		}
		
		if (plateformeService.recupererPlateformes().isEmpty()) {
			plateformeService.creerPlateforme(new Plateforme("Windows"));
			plateformeService.creerPlateforme(new Plateforme("MacOS"));
			plateformeService.creerPlateforme(new Plateforme("Playstation"));
			plateformeService.creerPlateforme(new Plateforme("Playstation 2"));
		}
		
		if (modeleEconomiqueService.recupererModelesEconomique().isEmpty()) {
			modeleEconomiqueService.creerModeleEconomique(new ModeleEconomique("FreeToPlay"));
			modeleEconomiqueService.creerModeleEconomique(new ModeleEconomique("Payant"));
		}
		
		if (joueurService.recupererJoueurs().isEmpty()) {
			joueurService.creerJoueur(new Joueur("florence", "12345", new Date(), true));
			joueurService.creerJoueur(new Joueur("linda", "12345", new Date(), false));
		}
		
		if (jeuService.recupererJeux().isEmpty()) {
			Jeu jeu1 = new Jeu();
			jeu1.setNom("Fortnite");
			jeu1.setDescription("Un jeu de battle royal");
			jeu1.setDateSortie(new Date());
			jeu1.setEditeur(editeurService.recupererEditeur(1l).get());
			jeu1.setModeleEconomique(modeleEconomiqueService.recupererModeleEconomique(1l).get());
			jeu1.setClassification(classificationService.recupererClassification(1l).get());
			jeu1.setGenre(genreService.recupererGenre(2l).get());
			List<Plateforme> plateformeJeu1 = new ArrayList<>();
			plateformeJeu1.add(plateformeService.recupererPlateforme(1l).get());
			plateformeJeu1.add(plateformeService.recupererPlateforme(3l).get());
			jeu1.setPlateformes(plateformeJeu1);
			jeuService.creerJeu(jeu1);
			jeuService.creerJeu(jeu1);
			jeuService.creerJeu(jeu1);
			
			Jeu jeu2 = new Jeu();
			jeu2.setNom("Call of Duty");
			jeu2.setDescription("Un jeu de guerre");
			jeu2.setDateSortie(new Date());
			jeu2.setEditeur(editeurService.recupererEditeur(2l).get());
			jeu2.setModeleEconomique(modeleEconomiqueService.recupererModeleEconomique(2l).get());
			jeu2.setClassification(classificationService.recupererClassification(2l).get());
			jeu2.setGenre(genreService.recupererGenre(1l).get());
			List<Plateforme> plateformeJeu2 = new ArrayList<>();
			plateformeJeu2.add(plateformeService.recupererPlateforme(2l).get());
			plateformeJeu2.add(plateformeService.recupererPlateforme(4l).get());
			jeu2.setPlateformes(plateformeJeu2);
			jeuService.creerJeu(jeu2);
		}
		
		if (avisService.recupererAviss().isEmpty()) {
			for (int i = 0; i < 10; i++) {
				avisService.creerAvis(new Avis(new Date(), (float) i, "Une description", jeuService.recupererJeu(1l).get(), joueurService.recupererJoueur(2l).get()));
			}
			for (int i = 10; i < 20; i++) {
				avisService.creerAvis(new Avis(new Date(), (float) i, "Une description", jeuService.recupererJeu(1l).get(), joueurService.recupererJoueur(1l).get()));
			}
		}
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder, WebRequest request) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(simpleDateFormat, true));
		binder.registerCustomEditor(List.class, "plateformes", new CustomCollectionEditor(List.class) {
			 @Override
			 public Object convertElement(Object objet) {
				 Long id = Long.parseLong((String) objet);
				 return plateformeService.recupererPlateforme(id).get();
			 }
		});
		binder.registerCustomEditor(Genre.class, "genre", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : genreService.recupererGenre(Long.parseLong(text)).get());
			}
		});
		binder.registerCustomEditor(Classification.class, "classification", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : classificationService.recupererClassification(Long.parseLong(text)).get());
			}
		});
		binder.registerCustomEditor(ModeleEconomique.class, "modeleEconomique", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : modeleEconomiqueService.recupererModeleEconomique(Long.parseLong(text)).get());
			}
		});
		binder.registerCustomEditor(Editeur.class, "editeur", new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				setValue((text.equals("")) ? null : editeurService.recupererEditeur(Long.parseLong(text)).get());
			}
		});
	}

}
