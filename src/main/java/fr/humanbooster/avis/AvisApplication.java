package fr.humanbooster.avis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class AvisApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(AvisApplication.class, args);
	}

}
