package fr.humanbooster.avis.service;

import java.util.List;
import java.util.Optional;

import fr.humanbooster.avis.business.Classification;

public interface ClassificationService {

	Classification creerClassification(Classification classification);
	
	Optional<Classification> recupererClassification(Long id);
	
	List<Classification> recupererClassifications();
	
	Classification majClassification(Classification classification);

	boolean supprimerClassification(Long id);
}
