package fr.humanbooster.avis.service;

import java.util.List;
import java.util.Optional;

import fr.humanbooster.avis.business.Plateforme;

public interface PlateformeService {

	Plateforme creerPlateforme(Plateforme plateforme);
	
	Optional<Plateforme> recupererPlateforme(Long id);
	
	List<Plateforme> recupererPlateformes();
	
	Plateforme majPlateforme(Plateforme plateforme);

	boolean supprimerPlateforme(Long id);
}
