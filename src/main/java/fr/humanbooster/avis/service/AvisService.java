package fr.humanbooster.avis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.humanbooster.avis.business.Avis;
import fr.humanbooster.avis.business.Jeu;
import fr.humanbooster.avis.business.Joueur;

public interface AvisService {

	Avis creerAvis(Avis avis);
	
	Optional<Avis> recupererAvis(Long id);
	
	List<Avis> recupererAviss();
	
	Page<Avis> recupererAviss(Pageable pageable);
	
	Avis majAvis(Avis avis);

	boolean supprimerAvis(Long id);

	List<Avis> recupererAvissParJeu(Jeu jeu);
	
	List<Avis> recupererAvisParJoueurOrdreDateEnvoiDesc(Joueur joueur);

	long nombreTotalAvis();
}
