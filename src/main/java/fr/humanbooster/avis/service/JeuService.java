package fr.humanbooster.avis.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.humanbooster.avis.business.Jeu;

public interface JeuService {

	Jeu creerJeu(Jeu jeu);
	
	Optional<Jeu> recupererJeu(Long id);
	
	List<Jeu> recupererJeux();
	
	List<Jeu> recupererJeuxEntreDeuxDates(Date dateDebut, Date dateFin);
	
	Page<Jeu> recupererJeux(Pageable pageable);
	
	Jeu majJeu(Jeu jeu);

	boolean supprimerJeu(Long id);
}
