package fr.humanbooster.avis.service;

import java.util.List;
import java.util.Optional;

import fr.humanbooster.avis.business.Joueur;

public interface JoueurService {

	Joueur creerJoueur(Joueur joueur);
	
	Optional<Joueur> recupererJoueur(Long id);
	
	Joueur recupererJoueurByPseudoAndMotDePasse(String pseudo, String motDePasse);
	
	List<Joueur> recupererJoueurs();
	
	Joueur majJoueur(Joueur joueur);

	boolean supprimerJoueur(Long id);
}
