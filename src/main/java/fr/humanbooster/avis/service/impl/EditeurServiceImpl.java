package fr.humanbooster.avis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.avis.business.Editeur;
import fr.humanbooster.avis.dao.EditeurDao;
import fr.humanbooster.avis.service.EditeurService;

@Service
public class EditeurServiceImpl implements EditeurService {

	@Autowired
	private EditeurDao editeurDao;
	
	@Override
	public Editeur creerEditeur(Editeur editeur) {
		return editeurDao.save(editeur);
	}

	@Override
	public Optional<Editeur> recupererEditeur(Long id) {
		return editeurDao.findById(id);
	}

	@Override
	public List<Editeur> recupererEditeurs() {
		return editeurDao.findAll();
	}

	@Override
	public Editeur majEditeur(Editeur editeur) {
		return editeurDao.save(editeur);
	}

	@Override
	public boolean supprimerEditeur(Long id) {
		Editeur editeur = recupererEditeur(id).get();
		if (editeur == null) {
			return false;
		} else {
			editeurDao.deleteById(id);
			return true;
		}
	}

}
