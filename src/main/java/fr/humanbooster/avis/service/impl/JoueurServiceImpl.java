package fr.humanbooster.avis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.avis.business.Joueur;
import fr.humanbooster.avis.dao.JoueurDao;
import fr.humanbooster.avis.service.JoueurService;

@Service
public class JoueurServiceImpl implements JoueurService {

	@Autowired
	private JoueurDao joueurDao;
	
	@Override
	public Joueur creerJoueur(Joueur joueur) {
		return joueurDao.save(joueur);
	}

	@Override
	public Optional<Joueur> recupererJoueur(Long id) {
		return joueurDao.findById(id);
	}
	
	@Override
	public Joueur recupererJoueurByPseudoAndMotDePasse(String pseudo, String motDePasse) {
		return joueurDao.findByPseudoAndMotDePasse(pseudo, motDePasse);
	}

	@Override
	public List<Joueur> recupererJoueurs() {
		return joueurDao.findAll();
	}

	@Override
	public Joueur majJoueur(Joueur joueur) {
		return joueurDao.save(joueur);
	}

	@Override
	public boolean supprimerJoueur(Long id) {
		Joueur joueur = recupererJoueur(id).get();
		if (joueur == null) {
			return false;
		} else {
			joueurDao.deleteById(id);
			return true;
		}
	}

}
