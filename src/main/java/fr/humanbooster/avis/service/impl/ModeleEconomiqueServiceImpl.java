package fr.humanbooster.avis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.avis.business.ModeleEconomique;
import fr.humanbooster.avis.dao.ModeleEconomiqueDao;
import fr.humanbooster.avis.service.ModeleEconomiqueService;

@Service
public class ModeleEconomiqueServiceImpl implements ModeleEconomiqueService {

	@Autowired
	private ModeleEconomiqueDao modeleEconomiqueDao;
	
	@Override
	public ModeleEconomique creerModeleEconomique(ModeleEconomique modeleEconomique) {
		return modeleEconomiqueDao.save(modeleEconomique);
	}

	@Override
	public Optional<ModeleEconomique> recupererModeleEconomique(Long id) {
		return modeleEconomiqueDao.findById(id);
	}

	@Override
	public List<ModeleEconomique> recupererModelesEconomique() {
		return modeleEconomiqueDao.findAll();
	}

	@Override
	public ModeleEconomique majModeleEconomique(ModeleEconomique modeleEconomique) {
		return modeleEconomiqueDao.save(modeleEconomique);
	}

	@Override
	public boolean supprimerModeleEconomique(Long id) {
		ModeleEconomique modeleEconomique = recupererModeleEconomique(id).get();
		if (modeleEconomique == null) {
			return false;
		} else {
			modeleEconomiqueDao.deleteById(id);
			return true;
		}
	}

}
