package fr.humanbooster.avis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.avis.business.Classification;
import fr.humanbooster.avis.dao.ClassificationDao;
import fr.humanbooster.avis.service.ClassificationService;

@Service
public class ClassificationServiceImpl implements ClassificationService {

	@Autowired
	private ClassificationDao classificationDao;
	
	@Override
	public Classification creerClassification(Classification classification) {
		return classificationDao.save(classification);
	}

	@Override
	public Optional<Classification> recupererClassification(Long id) {
		return classificationDao.findById(id);
	}

	@Override
	public List<Classification> recupererClassifications() {
		return classificationDao.findAll();
	}

	@Override
	public Classification majClassification(Classification classification) {
		return classificationDao.save(classification);
	}

	@Override
	public boolean supprimerClassification(Long id) {
		Classification classification = recupererClassification(id).get();
		if (classification == null) {
			return false;
		} else {
			classificationDao.deleteById(id);
			return true;
		}
	}

}
