package fr.humanbooster.avis.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.humanbooster.avis.business.Plateforme;
import fr.humanbooster.avis.dao.PlateformeDao;
import fr.humanbooster.avis.service.PlateformeService;

@Service
public class PlateformeServiceImpl implements PlateformeService {

	@Autowired
	private PlateformeDao plateformeDao;
	
	@Override
	public Plateforme creerPlateforme(Plateforme plateforme) {
		return plateformeDao.save(plateforme);
	}

	@Override
	public Optional<Plateforme> recupererPlateforme(Long id) {
		return plateformeDao.findById(id);
	}

	@Override
	public List<Plateforme> recupererPlateformes() {
		return plateformeDao.findAll();
	}

	@Override
	public Plateforme majPlateforme(Plateforme plateforme) {
		return plateformeDao.save(plateforme);
	}

	@Override
	public boolean supprimerPlateforme(Long id) {
		Plateforme plateforme = recupererPlateforme(id).get();
		if (plateforme == null) {
			return false;
		} else {
			plateformeDao.deleteById(id);
			return true;
		}
	}

}
