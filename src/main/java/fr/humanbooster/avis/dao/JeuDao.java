package fr.humanbooster.avis.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.avis.business.Jeu;

public interface JeuDao extends JpaRepository<Jeu, Long> {

	List<Jeu> findAllByDateSortieBetween(Date dateDebut, Date dateFin);
}
