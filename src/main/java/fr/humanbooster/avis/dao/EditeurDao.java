package fr.humanbooster.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.avis.business.Editeur;

public interface EditeurDao extends JpaRepository<Editeur, Long> {

}
