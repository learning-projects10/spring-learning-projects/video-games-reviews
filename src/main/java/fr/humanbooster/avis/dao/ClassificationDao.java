package fr.humanbooster.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.avis.business.Classification;

public interface ClassificationDao extends JpaRepository<Classification, Long> {

}
