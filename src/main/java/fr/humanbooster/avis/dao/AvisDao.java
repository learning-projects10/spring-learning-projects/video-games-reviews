package fr.humanbooster.avis.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.avis.business.Avis;
import fr.humanbooster.avis.business.Jeu;
import fr.humanbooster.avis.business.Joueur;

public interface AvisDao extends JpaRepository<Avis, Long> {

	List<Avis> findAllByJeu(Jeu jeu);
	
	List<Avis> findByJoueurOrderByDateEnvoiDesc(Joueur joueur);
}
