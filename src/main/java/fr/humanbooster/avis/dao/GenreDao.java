package fr.humanbooster.avis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.humanbooster.avis.business.Genre;

public interface GenreDao extends JpaRepository<Genre, Long> {

}
