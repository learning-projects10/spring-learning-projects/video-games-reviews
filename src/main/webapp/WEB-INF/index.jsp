<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Connexion</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
		
		<main>
			<h1>Bienvenue sur Avis de jeux vidéo !</h1>
			
			<c:if test="${notification ne null}">
				<p class="erreur">${notification}</p>
			</c:if>
			
	   		<form action="connexion" method="post">
				<div class="form-group">
					<label for="pseudo">Pseudo : </label>
					<input class="form-control" type="text" id="pseudo" name="PSEUDO" placeholder="Pseudo">
				</div>
				<div class="form-group">
					<label for="motDePasse">Mot de passe : </label>
					<input class="form-control" type="password" id="motDePasse" name="MOT_DE_PASSE" placeholder="Mot de passe">
				</div>
				<input class="btn btn-success my-2" type="submit" value="Connexion">
	   		</form>
	   		
	   		<a class="btn btn-primary" href="inscription">M'inscrire</a>
   		</main>
   		
	</body>
</html>