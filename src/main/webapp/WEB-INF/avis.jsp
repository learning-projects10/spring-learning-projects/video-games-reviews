<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Ajouter un avis</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
	
		<jsp:include page="componants/header.jsp"></jsp:include>
	
		<main>
			<h1>Ajout d'un avis</h1>
			
			<form:form modelAttribute="avis" action="ajouter-avis" method="post">
				
				<div class="form-group">
					<form:label path="jeu.id">Jeu : </form:label>
					<form:select class="form-control" path="jeu.id">
						<form:option value="">Jeu</form:option>
						<form:options items="${jeux}" itemValue="id" itemLabel="nom"/>
					</form:select>
					<form:errors path="jeu.id" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="description">Description :</form:label>
					<form:input class="form-control" path="description"/>
					<form:errors path="description" cssClass="erreur" />
				</div>
				
				<div class="form-group">
					<form:label path="note">Note :</form:label>
					<form:input type="number" class="form-control" path="note"/>
					<form:errors path="note" cssClass="erreur" />
				</div>
				
				<form:button class="btn btn-success my-3">Ajouter l'avis</form:button>
			</form:form>
			
			<a href="index" class="btn btn-primary">Liste des avis</a>
		</main>
	</body>
</html>