<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Liste des avis</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
	
		<jsp:include page="componants/header.jsp"></jsp:include>
		
		<h3 class="my-2">Liste des avis</h3>
		
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Date d'envoi<a href="?sort=dateEnvoi,asc"class="dropdown-toggle"></a><a href="?sort=dateEnvoi,desc"class="dropdown-toggle tourne"></a></th>
					<th>Nom du jeu<a href="?sort=jeu.nom,asc"class="dropdown-toggle"></a><a href="?sort=jeu.nom,desc"class="dropdown-toggle tourne"></a></th>
					<th>Pseudo du joueur<a href="?sort=joueur.pseudo,asc"class="dropdown-toggle"></a><a href="?sort=joueur.pseudo,desc"class="dropdown-toggle tourne"></a></th>
					<th>Note<a href="?sort=note,asc"class="dropdown-toggle"></a><a href="?sort=note,desc"class="dropdown-toggle tourne"></a></th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="avis" items="${avisParPages.content}">
					<tr>
						<td><fmt:formatDate dateStyle="short" timeStyle="short" type="both" value="${avis.dateEnvoi}"/></td>
						<td>${avis.jeu.nom}</td>
						<td>${avis.joueur.pseudo}</td>
						<td><fmt:formatNumber type="number" maxIntegerDigits="2" value="${avis.note}"/></td>
						<td>${avis.description}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<a href="ajouter-avis" class="btn btn-primary btn-sm">Ajouter un avis</a>
		<c:if test="${sessionScope.joueur.estAdministrateur eq true}">
			<a href="liste-jeux" class="btn btn-primary btn-sm">Liste des jeux</a>
		</c:if>

		<nav aria-label="Page navigation example" class="my-3">
			<ul class="pagination">
				<c:if test="${!avisParPages.isFirst()}">
			    	<li class="page-item">
			     		<a class="page-link double" href="accueil?page=0&sort=${sort}" aria-label="Previous">
			       	 		<span aria-hidden="true">&laquo;&laquo;</span>
			    		</a>
			    	</li>
			    	<li class="page-item">
			     		<a class="page-link" href="accueil?page=${avisParPages.number-1}&sort=${sort}" aria-label="Previous">
			       	 		<span aria-hidden="true">&laquo;</span>
			    		</a>
			    	</li>
				</c:if>
		    	<li class="page-item"><a class="page-link" href="#">${avisParPages.getNumber()+1}</a></li>
		    	<c:if test="${!avisParPages.isLast()}">
			    	<li class="page-item">
			      		<a class="page-link" href="accueil?page=${avisParPages.number+1}&sort=${sort}" aria-label="Next">
			      			<span aria-hidden="true">&raquo;</span>
			     		</a>
			  		</li>
			    	<li class="page-item">
			      		<a class="page-link double" href="accueil?page=${avisParPages.getTotalPages() - 1}&sort=${sort}" aria-label="Next">
			      			<span aria-hidden="true">&raquo;&raquo;</span>
			     		</a>
			  		</li>
		    	</c:if>
		  	</ul>
		</nav>
		
		<c:if test="${avisParPages.numberOfElements ne 0}">
			<p>Avis de ${avisParPages.size * avisParPages.number+1}
			 à ${(avisParPages.size * avisParPages.number+1) + avisParPages.numberOfElements-1} 
			 sur ${avisParPages.totalElements} avis</p>
		</c:if>
		
	</body>
</html>